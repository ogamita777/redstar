RedStar
===
[FightingICE](http://www.ice.ci.ritsumei.ac.jp/~ftgaic/index.htm)のAIです  
キャラクター：GARNET

基本方針
---
- CROUCH_Bの連続攻撃でENERGYを貯めてSTAND_D_DF_FC
- 敵のSTAND_D_DF_FCになるべく当たらないようにする
	- 回避行動 
- 自分のSTAND_D_DF_FCが当たる可能性を高くする
	- 敵キャラとの距離が近いときの謎の上下運動 
	- 端に向かって撃つのが駄目？
		- 端に向かって撃たないようにBACK_JUMP 

アルゴリズム
---
1. 自分がDOWNしている
	1. はい＞ジャンプ＆ガード
	2. いいえ＞次へ 
1. 敵がSTAND_D_DF_FCを撃つ可能性が高い？
	1. はい＞回避行動
	2. いいえ＞次へ 
1. 自分がAIRにいる？
	1. はい＞B攻撃
	2. いいえ＞次へ 
1. STAND_D_DF_FCを使える？
	1. はい
		1. 端に向かっている＞BACK_JUMP
		2. 端に向かっていない＞STAND_D_DF_FC
	2. いいえ＞次へ 
1. 敵との距離が遠い？
	1. はい＞FOR_JUMP
	2. いいえ＞次へ 
1. ちょっと右に進んで次へ
1. 距離が中途半端？
	1. はい＞STAND_FB
	2. いいえ＞CROUCH_B 

知見
---
- 攻撃の有効範囲などは各キャラのMotion.csvに書いてある(Webに載ってるのは古いのでFightingICEに入ってるのを見た方が良い)

疑問点
---
- STAND_D_DF_FCがキャンセルされる条件
- なぜCROUCH_Bは連続攻撃のスパンが短いのか

参考AI
---
SejongFighter  
PnumaSON3C_AI

