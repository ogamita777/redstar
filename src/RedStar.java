import enumerate.Action;
import gameInterface.AIInterface;

import java.util.HashMap;
import java.util.Random;

import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.Key;
import structs.MotionData;

import commandcenter.CommandCenter;

/**
 * 自分のSTAND_D_DF_FCをなるべく当て、敵のSTAND_D_DF_FCをなるべく当たらないようにするGARNETさんのAIを目指す
 * 攻撃の有効範囲などはMotion.csvから正しく計算して利用する
 * 
 * @author @ogamita777
 */
public class RedStar implements AIInterface {

  /** STAND_D_DF_FCを撃つのに必要なエネルギー */
  public final int STAND_D_DF_FC_ENERGY = 300;

  /** STAND_D_DF_FCの左X座標制限 */
  public final int STAND_D_DF_FC_LEFT_LIMIT = 0;

  /** STAND_D_DF_FCの右X座標制限 */
  public final int STAND_D_DF_FC_RIGHT_LIMIT = 550;

  /** STAND_D_DF_F発動に必要なエネルギー(ZEN,GARNET,LUDの順) */
  public final int[] STAND_D_DF_FC_ENEGRY = {300, 300, 400};

  /** 敵がSTAND_D_DF_FCを使ってくるかどうか */
  public boolean canFC = true;

  /** STAND_D_DF_FCの回避行動用フラグ */
  public boolean isFcFirst = true;

  /** STAND_D_DF_FCの回避行動時間を計測する */
  private long firstFcTime;

  /** キャラクターの名前を数値に変換する */
  HashMap<String, Integer> c2i = new HashMap<String, Integer>();

  /** 入力されるキー */
  Key key;

  /** ランダムな値 */
  Random rnd;

  /** コマンドを管理する */
  CommandCenter commandCenter;

  /** プレイヤー番号 */
  boolean playerNumber;

  /** ゲームデータ */
  GameData gameData;

  /** フレームデータ */
  FrameData frameData;

  @Override
  public void close() {}

  @Override
  public String getCharacter() {
    return CHARACTER_GARNET;
  }

  @Override
  public void getInformation(FrameData frameData) {
    this.frameData = frameData;
    this.commandCenter.setFrameData(this.frameData, playerNumber);
  }

  @Override
  public int initialize(GameData gameData, boolean playerNumber) {
    this.key = new Key();
    this.rnd = new Random();
    this.playerNumber = playerNumber;
    this.commandCenter = new CommandCenter();
    this.gameData = gameData;

    c2i.put("ZEN", 0);
    c2i.put("GARNET", 1);
    c2i.put("LUD", 2);

    return 0;
  }

  @Override
  public Key input() {
    return key;
  }

  @Override
  public void processing() {
    if (canProcessing(frameData)) {

      if (commandCenter.getskillFlag()) {
        key = commandCenter.getSkillKey();
      } else {
        key.empty();
        commandCenter.skillCancel();

        aiAction(); // AIの行動を実行する
      }
    }
  }

  /**
   * AIの行動
   */
  public void aiAction() {
    CharacterData myCharacter = commandCenter.getMyCharacter();
    CharacterData enemyCharacter = commandCenter.getEnemyCharacter();

    int myLeft = myCharacter.left;
    int myRight = myCharacter.right;
    int myTop = myCharacter.top;
    int myBottom = myCharacter.bottom;
    int myX = commandCenter.getMyX();
    int myY = commandCenter.getMyY();

    int enemyLeft = enemyCharacter.left;
    int enemyRight = enemyCharacter.right;
    int enemyTop = enemyCharacter.top;
    int enemyBottom = enemyCharacter.bottom;
    int enemyX = commandCenter.getEnemyX();
    int enemyY = commandCenter.getEnemyY();

    int myEnergy = commandCenter.getMyEnergy();
    boolean isMyFrontRight = myCharacter.isFront();
    String myState = myCharacter.getState().name();
    String enemyState = enemyCharacter.getState().name();
    String enemyAction = enemyCharacter.getAction().name();
    String enemyName = gameData.getOpponentName(playerNumber);
    int enemyEnergy = enemyCharacter.getEnergy();
    int enemyNumber = c2i.get(enemyName); // ZEN0,GARNET1,LUD2

    boolean isHitCROUCH_B = false; // 敵がCROUCH_Bの有効範囲にいるか
    boolean isHitSTAND_FB = false; // 敵がSTAND_FBの有効範囲にいるか
    boolean isHitSTAND_FC = false; // 敵がSTAND_FCの有効範囲にいるか

    Action myAct = commandCenter.getEnemyCharacter().getAction();

    MotionData myMotion = new MotionData();
    if (playerNumber) {
      myMotion = gameData.getPlayerTwoMotion().elementAt(myAct.ordinal());
    } else {
      myMotion = gameData.getPlayerOneMotion().elementAt(myAct.ordinal());
    }

    /*
     * System.out.println("My:" + myLeft + "," + myRight + "," + myTop + "," + myBottom + "," +
     * myCharacter.attack.getHitAreaNow().getL() + "," + myCharacter.attack.getHitAreaNow().getR() +
     * "," + myState + "," + myX + "," + myY + "," + myMotion.attackHit.getL() + "," +
     * myMotion.attackHit.getR()); System.out.println("Enemy:" + enemyLeft + "," + enemyRight + ","
     * + enemyTop + "," + enemyBottom + "," + enemyCharacter.attack.getHitAreaNow().getL() + "," +
     * enemyCharacter.attack.getHitAreaNow().getR() + "," + enemyState + "," + enemyX + "," +
     * enemyY);
     */

    int myHitLeft = isMyFrontRight ? myRight + (240 - 220) : myLeft - (300 - 220);
    int myHitRight = isMyFrontRight ? myRight + (300 - 220) : myLeft - (240 - 220);
    int myHitUp = myTop + (280 - 150);
    int myHitDown = myTop + (305 - 150);

    isHitCROUCH_B =
        (enemyLeft <= myHitRight) && (enemyRight >= myHitLeft) && (enemyTop <= myHitDown)
            && (enemyBottom >= myHitUp);

    int offsetFB = 0;

    myHitLeft = isMyFrontRight ? myRight + (280 - 220) : myLeft - (380 - 220) - offsetFB;
    myHitRight = isMyFrontRight ? myRight + (380 - 220) + offsetFB : myLeft - (280 - 220);

    isHitSTAND_FB = (enemyLeft <= myHitRight) && (enemyRight >= myHitLeft);


    myHitLeft = isMyFrontRight ? myRight + (240 - 220) : myLeft - (295 - 220) - 35 * 10;
    myHitRight = isMyFrontRight ? myRight + (295 - 220) + 35 * 10 : myLeft - (240 - 220);
    myHitUp = myTop + (180 - 150);
    myHitDown = myTop + (220 - 150);

    isHitSTAND_FC =
        (enemyLeft <= myHitRight) && (enemyRight >= myHitLeft) && (enemyTop <= myHitDown)
            && (enemyBottom >= myHitUp);

    // ダウンしているときはジャンプしてガード
    if (myState.equals("DOWN")) {
      commandCenter.commandCall("7 _ C");
      return;
    }

    // 敵がSTAND_D_DF_FCを撃ってきたら、敵はSTAND_D_DF_FCを撃つ可能性があるというフラグをたてる
    if (enemyAction.equals("STAND_D_DF_FC")) {
      canFC = true;
      isFcFirst = true;
    }

    // 必殺技対策用回避(最高で10秒続ける)
    if (enemyEnergy >= STAND_D_DF_FC_ENEGRY[enemyNumber] && canFC) {
      if (isFcFirst) {
        firstFcTime = frameData.getRemainingTime();
        isFcFirst = false;
        System.out.println(firstFcTime);
      }
      commandCenter.commandCall("FOR_JUMP");
    }

    if (!isFcFirst && canFC) {
      if (enemyEnergy < STAND_D_DF_FC_ENEGRY[enemyNumber]) {
        isFcFirst = true;
      }
      if (firstFcTime - frameData.getRemainingTime() >= 10000) {
        canFC = false;
        isFcFirst = true;
      }
    }

    // 空中にいる場合はB攻撃しておく
    if (myState.equals("AIR")) {
      key.B = true;
      return;
    }

    // 必殺技(STAND_D_DF_FC)の当たる可能性が高い場合は発動させる
    if (myEnergy >= STAND_D_DF_FC_ENERGY) { // 必殺技を発動できるエネルギーを持っている
      if (isHitSTAND_FC) { // 攻撃が相手に当たる可能性が高い
        if ((myX <= STAND_D_DF_FC_LEFT_LIMIT && isMyFrontRight) // 敵に必殺技が当たらない状況ではない
            || (myX >= STAND_D_DF_FC_RIGHT_LIMIT && !isMyFrontRight)
            || (myX > STAND_D_DF_FC_LEFT_LIMIT && myX < STAND_D_DF_FC_RIGHT_LIMIT)) {
          if (enemyState.equals("AIR")) { // 相手が空中にいるならなにもしない
          } else if (enemyAction.endsWith("RECOV")) { // 相手がRECOV中なら必ず撃つ
            commandCenter.commandCall("STAND_D_DF_FC");
            return;
          } else {
            // TODO:相手攻撃中は攻撃しないようにする
            System.out.println(enemyAction);
            commandCenter.commandCall("STAND_D_DF_FC");
            return;
          }
        } else { // 敵に必殺技が当たらない状況の場合
          commandCenter.commandCall("BACK_JUMP");
        }
      }
    }

    // 距離が遠い場合はジャンプ
    if (!isHitCROUCH_B && !isHitSTAND_FB) {
      commandCenter.commandCall("FOR_JUMP");
      return;
    }

    // 若干前に進む
    if (commandCenter.getMyCharacter().isFront()) {
      key.R = true;
    } else {
      key.L = true;
    }

    // CROUCH_Bが届かなくて、STAND_FBが届くならSTAND_FB
    if (isHitSTAND_FB && !isHitCROUCH_B) {
      commandCenter.commandCall("STAND_FB");
      return;
    }

    // 上記以外の場合はCROUCH_B
    commandCenter.commandCall("CROUCH_B");
    return;
  }

  /**
   * processing可能かを判定するメソッド
   * 
   * @return processing可能か
   */
  public boolean canProcessing(FrameData frameData) {
    return !frameData.getEmptyFlag() && frameData.getRemainingTime() > 0;
  }
}
